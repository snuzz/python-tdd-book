[1] sudo python3 deploy_nginx_conf.run_me.py

[2] sudo rm -rf /etc/nginx/sites-enabled/superlists.byteopia.com

[3] sudo python3 activate_nginx.run_me.py

[4] sudo python3 deploy_systemd_service.run_me.py

[5] sudo systemctl daemon-reload

[6] sudo systemctl reload nginx

[7] sudo systemctl enable gunicorn-superlists.byteopia.com

[8] sudo systemctl start gunicorn-superlists.byteopia.com

[9] sudo reboot
